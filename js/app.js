const container = document.querySelector('.container');
const resultado = document.querySelector('#resultado');
const formulario = document.querySelector('#formulario');

document.addEventListener("DOMContentLoaded", main);

function main() {
  window.addEventListener('load', () => {
    formulario.addEventListener("submit", buscar);
  })
}
function buscar(e) {
  e.preventDefault();
  
  const ciudad = document.querySelector('#ciudad').value.trim();
  const pais = document.querySelector('#pais').value.trim();

  if (ciudad === '' || pais === '') {
    alerta('Todos los campos son obligatorios', 'error');
    return;
  }

  consultarAPI(ciudad, pais);
}

function alerta(texto, tipo) {
  const alerta = document.createElement('div');
  alerta.classList.add('bg-red-100', 'border-red-100', 'text-red-500', 'px-4', 'py-3', 'rounded', 'max-w-md', 'mx-auto', 'mt-6', 'text-center');
  alerta.textContent = texto;

  container.appendChild(alerta);

  setTimeout(() => {
    alerta.remove();
  }, 5000);
}

function consultarAPI(ciudad, pais) {
  const key = 'a8f8d50d111685e45d44fcf54f9f1f13';
  const url = `https://api.openweathermap.org/data/2.5/weather?q=${ciudad},${pais}&appid=${key}`;

  spinner();

  fetch(url)
    .then( respuesta => {
      return respuesta.json();
    })
      .then( data => {
        if (parseInt(data.cod) === 401) {
          alerta('API key incorrecta', 'error');
        } else if (parseInt(data.cod) === 404) {
          alerta('Ciudad no encontrada', 'error');
        } else {
          mostarClima(data);
        }
      })
    .catch( error => {
      alerta(error)
    });
}

function mostarClima({ name, main: { temp, temp_max, temp_min }}) {

  limpiarHTML();
  
  const nombre = document.createElement('p');
  nombre.innerText = `${name}`;
  nombre.classList.add('font-bold', 'text-2xl');

  const actual = document.createElement('p');
  actual.innerText = `${KaC(temp)}°`;
  actual.classList.add('font-bold', 'text-5xl');

  const maxima = document.createElement('p');
  maxima.innerText = `Maxima: ${KaC(temp_max)}°`;
  maxima.classList.add('font-bold', 'text-2xl');

  const minima = document.createElement('p');
  minima.innerText = `Minima: ${KaC(temp_min)}°`;
  minima.classList.add('font-bold', 'text-2xl');
  
  const div = document.createElement('div');
  div.classList.add('text-center', 'text-white');

  div.appendChild(nombre);
  div.appendChild(actual);
  div.appendChild(maxima);
  div.appendChild(minima);

  resultado.appendChild(div);
}

function limpiarHTML() {
  while (resultado.firstChild) {
    resultado.removeChild(resultado.firstChild);
  }
}

/*
==================================================
          Helpers
==================================================
*/
const KaC = (kelvin) => parseInt(kelvin - 273.15);

function spinner() {
  limpiarHTML();
  const spinner = document.createElement('div');
  spinner.classList.add('sk-fading-circle');
  spinner.innerHTML = `
    <div class="sk-circle1 sk-circle"></div>
    <div class="sk-circle2 sk-circle"></div>
    <div class="sk-circle3 sk-circle"></div>
    <div class="sk-circle4 sk-circle"></div>
    <div class="sk-circle5 sk-circle"></div>
    <div class="sk-circle6 sk-circle"></div>
    <div class="sk-circle7 sk-circle"></div>
    <div class="sk-circle8 sk-circle"></div>
    <div class="sk-circle9 sk-circle"></div>
    <div class="sk-circle10 sk-circle"></div>
    <div class="sk-circle11 sk-circle"></div>
    <div class="sk-circle12 sk-circle"></div>
  `;
  resultado.appendChild(spinner);
}